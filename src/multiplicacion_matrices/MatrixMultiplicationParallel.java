package multiplicacion_matrices;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Random;

public class MatrixMultiplicationParallel {
	
	private final static Random generator = new Random();
	
	public static void llenar_matriz(int[][] matriz, int n) 
	{

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++) 
            {
                matriz[i][j] = generator.nextInt(20);
                //System.out.println(matriz[i][j]);
                //System.out.println("\t");
            }			
        }
	}

	
	public static void imprimir (int [][] matriz)
	
	{
		for (int x=0; x < matriz.length ;x++)
		{
			for(int y = 0; y < matriz.length ;y++)
			{
				System.out.print(matriz[x][y]+" ");
				//if(y != n) System.out.print("\t");
			}
			System.out.println("\t");
		}
		
	}

	 public static void main(String[] args) throws IOException 
	 {
		 FileWriter archivo= null;
		 PrintWriter escritor= null;
		 int n = 1000;
		 
		 try {
			 archivo= new FileWriter("F://salida_2.txt");
			 escritor= new PrintWriter(archivo);
			 for(int i=2; i<=n;i++)
			 {
				 System.out.println(i);
				 int [][]m1 = new int [i][i];
				 int [][]m2= new int [i][i];
				 llenar_matriz(m1,i);
				 llenar_matriz (m2,i);
				 escritor.print(i+" ");
				 //Date start = new Date();
				 long inicio = System.nanoTime();
				 int[][] result = new int[i][i];
				 thread_paralelo.multiply(m1, m2, result);
				 long fin = System.nanoTime();
				 double tiempo = (double)((fin-inicio)/10000);
				 escritor.print(tiempo + "\n");
				 System.out.println(tiempo + " segundos");
				}	 
		 }
		 catch (Exception e) {
			 System.out.println("Error: "+ e.getMessage()); 
		 }
		 
		 finally{
				archivo.close();
			} 



	 }

	}