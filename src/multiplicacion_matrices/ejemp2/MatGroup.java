package multMatrices;
import java.util.Date;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class MatGroup {
static int num = 2;
	public static void main(String[] args) throws IOException
	{
		for(int i=0; i < num; i++) {
			MatrixTest mat = new MatrixTest();
			mat.set_n(i);
			Date start = new Date();
			mat.main_mat();
			Date end = new Date();
			System.out.println("\nTime taken in milli seconds: " + (end.getTime() - start.getTime()));
			
		}
	}
	
}
