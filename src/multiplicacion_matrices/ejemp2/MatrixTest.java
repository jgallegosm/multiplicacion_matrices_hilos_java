package multMatrices;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MatrixTest {

static int n;

public void set_n(int num) {
	n=num;
}

//Creating the matrix
static int[][] mat = new int[n][n];
static int[][] mat2 = new int[n][n];
static int[][] result = new int[n][n];

public void main_mat(){
	

    //Creating the object of random class
    Random rand = new Random();


    //Filling first matrix with random values
    for (int i = 0; i < mat.length; i++) {
        for (int j = 0; j < mat[i].length; j++) {
            mat[i][j]=rand.nextInt(10);
        }
    }

    //Filling second matrix with random values
    for (int i = 0; i < mat2.length; i++) {
        for (int j = 0; j < mat2[i].length; j++) {
            mat2[i][j]=rand.nextInt(10);
        }
    }

    try{
        //Object of multiply Class
        Multiply multiply = new Multiply(n,n);
        
        List<Thread> threads = new ArrayList<>(n);
        //Threads
        for (int x = 0; x < n; x++) {
        	Thread t = new Thread(new MatrixMultiplier(multiply));
        	   t.start();
        	   threads.add(t);
        }
        
        /*
        MatrixMultiplier thread1 = new MatrixMultiplier(multiply);
        MatrixMultiplier thread2 = new MatrixMultiplier(multiply);
        MatrixMultiplier thread3 = new MatrixMultiplier(multiply);

        //Implementing threads
        Thread th1 = new Thread(thread1);
        Thread th2 = new Thread(thread2);
        Thread th3 = new Thread(thread3);

        //Starting threads
        th1.start();
        th2.start();
        th3.start();*/
        
        for (Thread t : threads) {
        	   t.join();
        	}

        /*th1.join();
        th2.join();
        th3.join();*/

    }catch (Exception e) {
        e.printStackTrace();
    }

    //Printing the result
    System.out.println("\n\nResult:");
    for (int i = 0; i < result.length; i++) {
        for (int j = 0; j < result[i].length; j++) {
            System.out.print(result[i][j]+" ");
        }
        System.out.println();
    }
  }//End main

  }//End Class

   //Multiply Class
   class Multiply extends MatrixTest {

private int i;
private int j;
private int chance;

public Multiply(int i, int j){
    this.i=i;
    this.j=j;
    chance=0;
}

//Matrix Multiplication Function
public synchronized void multiplyMatrix(){

    int sum=0;
    int a=0;
    for(a=0;a<i;a++){
        sum=0;
        for(int b=0;b<j;b++){
            sum=sum+mat[chance][b]*mat2[b][a];
        }
        result[chance][a]=sum;
    }

    if(chance>=i)
        return;
    chance++;
}
}//End multiply class

//Thread Class
     class MatrixMultiplier implements Runnable {

private final Multiply mul;

public MatrixMultiplier(Multiply mul){
    this.mul=mul;
}

@Override
public void run() {
    mul.multiplyMatrix();
}
}