package multiplicacion_matrices;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class thread_paralelo extends Thread
{
	
	public double tiempo_total;

	// creating 10 threads and waiting for them to complete then again repeat steps.
	public static void multiply(int[][] matrix1, int[][] matrix2, int[][] result) 
	{
		List <Thread> hilos = new ArrayList<>(); 
		//List threads = new ArrayList<>();
		int rows1 = matrix1.length;
		for (int i = 0; i < rows1; i++) 
		{
			multi_thread task = new multi_thread(result, matrix1, matrix2, i);
			Thread thread = new Thread(task);
			thread.start();
			hilos.add(thread);
			if (hilos.size() % 10 == 0) 
			{
				waitForThreads(hilos);
			 }
		}
	 }
	
	private static void waitForThreads(List<Thread> hilos) 
	{
		for (Thread thread : hilos) 
		{
			try 
			{
				thread.join();
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
	  }
	  hilos.clear();
	 }
}


